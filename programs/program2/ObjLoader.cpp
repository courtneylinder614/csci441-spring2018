#include <vector>
#include <cstdio>
#include "ObjLoader.h"

std::vector<std::string> split(std::string str, char delimiter) {
    std::vector<std::string> out = std::vector<std::string>();

    std::string current = std::string();

    for (auto c : str) {
        if (c == delimiter) {
            out.push_back(current);
            current = "";
        } else {
            current += c;
        }
    }

    if (!current.empty()) {
        out.push_back(current);
    }

    return out;
}

std::vector<float> ObjLoader::loadObj(std::string fileName) {
    // Read File
    std::ifstream file;
    file.open(fileName);

    std::vector<float> vert;
    std::vector<float> norm;
    std::vector<float> texture;

    std::vector<int> indices;

    std::string line;
    while (std::getline(file, line)) {

        auto values = split(line, ' ');

        if (values.empty()) continue;

        // Face
        if (values[0] == "f") {
            for (int i = 1; i < 4; i++) {
                //std::cout<<values[i]<<std::endl;
                indices.push_back(std::stoi(values[i]) - 1);
            }
        // Vert
        } else if (values[0] == "v") {
            for (int i = 1; i < 4; i++) {
                vert.push_back(std::stof(values[i]));
            }
        // Normal
        } else if (values[0] == "vn") {
            for (int i = 1; i < 4; i++) {
                norm.push_back(std::stof(values[i]));
            }
        // Tex
        } else if (values[0] == "vt") {
            for (int i = 1; i < 3; i++) {
                texture.push_back(std::stof(values[i]));
            }
        }
    }

    // Put all the info into a single list
    std::vector<float> out = std::vector<float>();

    bool hasNormals = !norm.empty();
    bool hasTextureCoords = !texture.empty();

    int stride = 3 + (hasNormals ? 3 : 0) + (hasTextureCoords ? 2 : 0);

    for (int index : indices) {
        for (int j = 0; j < 3; j++) {
            out.push_back(vert[index * stride + j]);
        }

        if (hasNormals) {
            for (int j = 0; j < 3; j++) {
                out.push_back(norm[index * stride + 3 + j]);
            }
        }

        if (hasTextureCoords) {
            for (int j = 0; j < 2; j++) {
                out.push_back(texture[index * stride + (hasNormals ? 6 : 3) + j]);
            }
        }
    }

    return out;
}

std::vector<float> ObjLoader::loadMaze(std::string fileName) {
    // Read File
    std::ifstream file;
    file.open(fileName);

    std::vector<float> vert;
    std::vector<float> norm;
    std::vector<float> texture;

    std::vector<int> vertexIndices;
    std::vector<int> uvIndices;
    std::vector<int> normalIndices;
    std::vector<int> indices;

    std::string line;
    while (std::getline(file, line)) {
        auto values = split(line, ' ');

        if (values.empty()) continue;

        if(values[0] == "g"){
          continue;
        }
        // Face
        if (values[0] == "f") {
            if(values.size() == 4){
              for (int i = 1; i < 4; i++) {
                auto indice = split(values[i], '/');
                vertexIndices.push_back(std::stoi(indice[0]));
                uvIndices.push_back(std::stoi(indice[1]));
                normalIndices.push_back(std::stoi(indice[2]));
              }
            }
            else if(values.size() == 5){
              for (int i = 1; i < 5; i++) {
                auto indice = split(values[i], '/');
                vertexIndices.push_back(std::stoi(indice[0]));
                uvIndices.push_back(std::stoi(indice[1]));
                normalIndices.push_back(std::stoi(indice[2]));
            }
          }
        }
        // Vert
       else if (values[0] == "v") {
            for (int i = 1; i < 4; i++) {
                vert.push_back(std::stof(values[i]));
            }
        // Normal
        } else if (values[0] == "vn") {
            for (int i = 1; i < 4; i++) {
                norm.push_back(std::stof(values[i]));
            }
        // Tex
        } else if (values[0] == "vt") {
            for (int i = 1; i < 3; i++) {
                texture.push_back(std::stof(values[i]));
            }
        }
    }

    // Put all the info into a single list
    std::vector<float> out = std::vector<float>();

    bool hasNormals = !norm.empty();
    bool hasTextureCoords = !texture.empty();

    int stride = 3 + (hasNormals ? 3 : 0) + (hasTextureCoords ? 2 : 0);

    for (int i = 0; i < vertexIndices.size(); i++) {
      for(int j = 0; j < 3; j++){
        out.push_back(vert[vertexIndices[i]*3+j-3]);
      }
    }

    return out;
}
