/*
 * Graphics 441, Program 2
 * Created By: Courtney Linder
 */

 /*
 * I had help for the Phong shading from Dan Church.
 * I had help for some of the math for camera movement
 * from stackoverflow.com.
 */

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"
#include "vec3.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

Camera camera;
//boolean to switch between camera views
bool firstPerson = true;
//global eye and target variables
float iX =0, iY = -1.25, iZ = 2, tX = sin(0.01), tY = -1, tZ = -cos(0.01);
//camera angle variable
float camAngle = 0;
// set the light position
Vector lightPos(3.75f, 3.75f, 4.0f);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(Matrix& model, GLFWwindow *window) {
    Matrix trans;
    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;
    //if we aren't in frist person mode, then we need to use AWSD keys
    //we also just adjust z and x variables by TRANS amout=nt.
    if(firstPerson == false){
      if (isPressed(window, GLFW_KEY_W)) {
        iZ -= TRANS;
        trans.translate(0,0,-(TRANS));
      }
      else if (isPressed(window, GLFW_KEY_S)) {
        iZ += TRANS;
        trans.translate(0,0,TRANS);
      }
      else if (isPressed(window, GLFW_KEY_A)) {
        iX -= TRANS;
        trans.translate(-TRANS,0,0);
      }
      else if (isPressed(window, GLFW_KEY_D)) {
        iX += TRANS;
        trans.translate(TRANS,0,0);
      }
    }else{
      //if we are in first person mode, use arrow keys for movement
      //also, need to be able to rotate the camera correctly
      if (isPressed(window, GLFW_KEY_UP)) {
        //set eye x and z variable to TRANS multiplied by target x/z
        iX += tX * TRANS;
        iZ += tZ * TRANS;
        camera.eye = Vector(iX, iY, iZ);
        camera.origin = Vector(iX+tX,tY,iZ+tZ);
        model.values[14] = camera.eye.z();
        model.values[12] = camera.eye.x();
        //lightPos = camera.eye;
      }
      else if (isPressed(window, GLFW_KEY_DOWN)) {
        //set eye x and z variable to TRANS multiplied by target x/z
        iX -= tX * TRANS;
        iZ -= tZ * TRANS;
        camera.eye = Vector(iX, iY, iZ);
        camera.origin = Vector(iX+tX,tY,iZ+tZ);
        model.values[14] = camera.eye.z();
        model.values[12] = camera.eye.x();
        //lightPos = camera.eye;
      }
      else if (isPressed(window, GLFW_KEY_LEFT)) {
        //decrease camera angle
        camAngle -= 0.01f;
        //find sin and -cos of camera angle for target/what to look at
        tX = sin(camAngle);
        tZ = -cos(camAngle);
        //just update what we are looking at, don't actually need to move
        //except for the first time since I am originally behind the duck
        camera.origin = Vector(iX+tX,tY,iZ+tZ);
        camera.eye = Vector(iX, iY, iZ);
      }
      else if (isPressed(window, GLFW_KEY_RIGHT)) {
        //increase camera angle
        camAngle += 0.01f;
        //find sin and -cos of camera angle for target/what to look at
        tX = sin(camAngle);
        tZ = -cos(camAngle);
        //just update what we are looking at, don't actually need to move
        //except for the first time since I am originally behind the duck
        camera.origin = Vector(iX+tX, tY, iZ+tZ);
        camera.eye = Vector(iX, iY, iZ);
      }
    }
    // SCALE
    if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    return trans*model;
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window);
}

void keycallback(GLFWwindow *window, int key, int scancode, int action, int mode) {
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE) {
        firstPerson = !firstPerson;
        if(firstPerson == false){
          //set camera above maze
          camera.eye = Vector(0, 3, 0);
          //have camera look down
          camera.origin = Vector(0, 0, -1);
          camera.up = Vector(0,1.5,0);
          //switch to orthogonal projection for cleaner look
          Matrix projection;
          projection.ortho(-3,3,-3,3,-1,100);
          camera.projection = projection;
          //lightPos = Vector(3.75f, 3.75f, 4.0f);
        }else{
          //swithc back to perspective projection
          Matrix projection;
          projection.perspective(45, 1, .01, 10);
          //set the camera back at the object position
          camera.eye = Vector(iX, iY, iZ);
          //have the camera look at the correct object
          camera.origin = Vector(iX+tX, tY, iZ+tZ);
          camera.up = Vector(0, 1, 0);
          camera.projection = projection;
          //lightPos = camera.eye;
        }
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create obj
    Model obj(
            Obj("../models/duck.obj", true).coords,
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix model_scale, model_trans;
    //scale and translate object initially
    model_scale.scale(0.0002, 0.0002, 0.0002);
    model_trans.translate(0, -1.75, 1.75);
    obj.model = model_trans*model_scale;

    //create maze
    Model maze(
            Obj("../models/maze.obj").coords,
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix maze_trans;
    maze_trans.translate(0, -2, -2);
    maze.model = maze_trans;

    // create a renderer
    Renderer renderer;

    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    //Matrix view;
    camera.projection = projection;
    camera.eye = Vector(iX, iY+.50, 3);
    camera.origin = Vector(0, 0, 0);
    camera.up = Vector(0, 1, 0);
    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    glfwSetKeyCallback(window, keycallback);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        lightPos = Vector(obj.model.values[12], obj.model.values[13], obj.model.values[14]);
        processInput(obj.model, window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the object and the maze
        renderer.render(camera, obj, lightPos);
        renderer.render(camera, maze, lightPos);

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
