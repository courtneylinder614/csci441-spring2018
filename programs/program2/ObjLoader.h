#ifndef GLFW_EXAMPLE_OBJLOADER_H
#define GLFW_EXAMPLE_OBJLOADER_H

#include <string>
#include <fstream>
#include <iostream>

class ObjLoader {
public:
    static std::vector<float> loadObj(std::string file);
    static std::vector<float> loadMaze(std::string file);
};


#endif
