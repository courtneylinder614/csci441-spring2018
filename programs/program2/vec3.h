#ifndef Vector3
#define Vector3

#include <cmath>

#include <iostream>

class vec3 {
public:
    float x, y, z;

    const static vec3 zero;

    vec3() = default;
    vec3(float x, float y, float z);

    vec3 operator/(float amount);
    vec3 operator*(float amount);
    vec3 operator-(vec3 other);
    vec3 operator+(vec3 other);

    float dot(vec3 other);
    float length();
    vec3 normalized();
    vec3 cross(vec3 other);

    friend bool operator==(const vec3& t, const vec3& other) {
        return t.x == other.x && t.y == other.y && t.z == other.z;
    }

    friend std::ostream& operator<<(std::ostream& os, const vec3& dt) {
        os << "[X: " << dt.x << ", Y: " << dt.y << ", Z: " << dt.z << "]";
        return os;
    }
};

#endif
