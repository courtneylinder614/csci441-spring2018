#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>

#include "vec3.h"

#include "ObjLoader.h"

template <typename T, typename N, typename C>
void add_vertex(T& coords, const N& x, const N& y, const N& z,
        const C& r, const C& g, const C& b, bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    coords.push_back(x);
    coords.push_back(y);
    coords.push_back(z);
    coords.push_back(r*noise);
    coords.push_back(g*noise);
    coords.push_back(b*noise);
}

// Gets the vertices only from a list containing vert & colors
std::vector<float> getVertices(std::vector<float> vnc) {
    std::vector<float> out = std::vector<float>();

    for (int i = 0; i < vnc.size() / 6; i++) {
        out.push_back(vnc[i * 6 + 0]);
        out.push_back(vnc[i * 6 + 1]);
        out.push_back(vnc[i * 6 + 2]);
    }

    return out;
}

std::vector<float> getColors(std::vector<float> vnc) {
    std::vector<float> out = std::vector<float>();

    for (int i = 0; i < vnc.size() / 6; i++) {
        out.push_back(vnc[3 + i * 6 + 0]);
        out.push_back(vnc[3 + i * 6 + 1]);
        out.push_back(vnc[3 + i * 6 + 2]);
    }

    return out;
}

std::vector<float> zip(std::vector<float> vert, std::vector<float> color, std::vector<float> normals) {
    std::vector<float> out = std::vector<float>();

    for (int i = 0; i < vert.size() / 3; i++) {
        out.push_back(vert[i * 3 + 0]);
        out.push_back(vert[i * 3 + 1]);
        out.push_back(vert[i * 3 + 2]);

        out.push_back(color[i * 3 + 0]);
        out.push_back(color[i * 3 + 1]);
        out.push_back(color[i * 3 + 2]);

        out.push_back(normals[i * 3 + 0]);
        out.push_back(normals[i * 3 + 1]);
        out.push_back(normals[i * 3 + 2]);
    }

    return out;
}

std::vector<float> zip(std::vector<float> vert, std::vector<float> color) {
    std::vector<float> out = std::vector<float>();

    for (int i = 0; i < vert.size() / 3; i++) {
        out.push_back(vert[i * 3 + 0]);
        out.push_back(vert[i * 3 + 1]);
        out.push_back(vert[i * 3 + 2]);

        out.push_back(color[i * 3 + 0]);
        out.push_back(color[i * 3 + 1]);
        out.push_back(color[i * 3 + 2]);
    }

    return out;
}

std::vector<float> calculateSmoothNormals(std::vector<float> vert) {
    std::vector<float> normals;

    // For each face
    for (int i = 0; i < vert.size() / 9; i++) {
        // For each vertex of each face find the faces it is connected to
        for (int j = 0; j < 3; j++) {
            std::vector<vec3> faces = std::vector<vec3>();
            // Include the face we are currently looking at
            faces.emplace_back(vert[i * 9 + 0], vert[i * 9 + 1], vert[i * 9 + 2]);
            faces.emplace_back(vert[i * 9 + 3], vert[i * 9 + 4], vert[i * 9 + 5]);
            faces.emplace_back(vert[i * 9 + 6], vert[i * 9 + 7], vert[i * 9 + 8]);

            // Our current vertex
            vec3 v3 = vec3(vert[i * 9 + j * 3 + 0], vert[i * 9 + j * 3 + 1], vert[i * 9 + j * 3 + 2]);

            // Look at all other faces (starting at the next one (ie i + 9))
            for (int m = i + 1; m < vert.size() / 9; m++) {
                // A list of vertices connected to each face
                std::vector<vec3> vec3s = std::vector<vec3>();

                // If we had a match
                bool found = false;
                // Look through each vertex individually
                for (int k = 0; k < 3; k++) {
                    // Add it to our list
                    vec3 v4 = vec3(vert[m * 9 + k * 3 + 0], vert[m * 9 + k * 3 + 1], vert[m * 9 + k * 3 + 2]);
                    vec3s.push_back(v4);

                    // If any match, set found to true
                    if (v3 == v4) {
                        found = true;
                    }
                }

                // If found, add all vertices to our faces list
                if (found) {
                    for (vec3 v5 : vec3s) {
                        faces.push_back(v5);
                    }
                }
            }

            // At this point, we have `faces` - a list of all the faces connected to a vertex

            // Find the normal for each face
            std::vector<vec3> averageNormals = std::vector<vec3>();
            for (int t = 0; t < faces.size() / 3; t++) {
                vec3 v1 = faces[t * 3 + 0];
                vec3 v2 = faces[t * 3 + 1];
                vec3 v3 = faces[t * 3 + 2];

                vec3 n1 = (v2 - v1).cross((v3 - v1));
                vec3 n2 = (v3 - v2).cross((v1 - v2));
                vec3 n3 = (v1 - v3).cross((v2 - v3));

                vec3 n4 = (n1 + n2 + n3) / 3;

                averageNormals.push_back(n4);
            }

            // Average all the face normals into a single normal
            vec3 finalNormal = vec3::zero;
            for (auto averageNormal : averageNormals) {
                finalNormal = finalNormal + averageNormal;
            }
            finalNormal = (finalNormal / averageNormals.size()).normalized();

            normals.push_back(finalNormal.x);
            normals.push_back(finalNormal.y);
            normals.push_back(finalNormal.z);
        }
    }

    return normals;
}

std::vector<float> calculateFlatNormals(std::vector<float> vert) {
    std::vector<float> normals;
    for (int i = 0; i < vert.size() / 9; i++) {
        vec3 v1 = vec3(vert[i * 9], vert[i * 9 + 1], vert[i * 9 + 2]);
        vec3 v2 = vec3(vert[3 + i * 9], vert[3 + i * 9 + 1], vert[3 + i * 9 + 2]);
        vec3 v3 = vec3(vert[6 + i * 9], vert[6 + i * 9 + 1], vert[6 + i * 9 + 2]);

        vec3 n1 = (v2 - v1).cross((v3 - v1)).normalized();
        vec3 n2 = (v3 - v2).cross((v1 - v2)).normalized();
        vec3 n3 = (v1 - v3).cross((v2 - v3)).normalized();

        vec3 n4 = (n1 + n2 + n3) / 3;

        normals.push_back(n4.x);
        normals.push_back(n4.y);
        normals.push_back(n4.z);

        normals.push_back(n4.x);
        normals.push_back(n4.y);
        normals.push_back(n4.z);

        normals.push_back(n4.x);
        normals.push_back(n4.y);
        normals.push_back(n4.z);
    }

    return normals;
}

std::vector<float> calculateFullVertInfo(std::vector<float> coords) {
    std::vector<float> vert = getVertices(coords);
    std::vector<float> colors = getColors(coords);
    return zip(vert, colors, calculateSmoothNormals(vert));
}

class Obj {
public:
    std::vector<float> coords;
    Obj(std::string path, bool isSmooth) {
        std::vector<float> coord = ObjLoader::loadObj(path);

        std::vector<float> color = std::vector<float>();
        for (int i = 0; i < coord.size() / 3; i++) {
            color.push_back(1);
            color.push_back(0);
            color.push_back(0);
        }

        if (isSmooth) {
            coords = zip(coord, color, calculateSmoothNormals(coord));
        } else {
            coords = zip(coord, color, calculateFlatNormals(coord));
        }
    }
    Obj(std::string path) {
        std::vector<float> coord = ObjLoader::loadMaze(path);
        std::vector<float> color = std::vector<float>();
        for (int i = 0; i < coord.size() / 3; i++) {
            color.push_back(0.5);
            color.push_back(0.5);
            color.push_back(0.5);
        }
        coords = zip(coord,color, calculateFlatNormals(coord));

    }
};

class DiscoCube {
public:
    std::vector<float> coords;
    DiscoCube() : coords {
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,
        0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
        0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
        0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
    } {
        coords = calculateFullVertInfo(coords);
    }

};

#endif
