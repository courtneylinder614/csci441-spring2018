#include "vec3.h"

const vec3 vec3::zero = vec3(0, 0, 0);

vec3::vec3(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

vec3 vec3::operator/(float amount) {
    return {x / amount, y / amount, z / amount};
}

vec3 vec3::operator*(float amount) {
    return {x * amount, y * amount, z * amount};
}

vec3 vec3::operator-(vec3 other) {
    return {x - other.x, y - other.y, z - other.z};
}

vec3 vec3::operator+(vec3 other) {
    return {x + other.x, y + other.y, z + other.z};
}

float vec3::dot(vec3 other) {
    return x * other.x + y * other.y + z * other.z;
}

float vec3::length() {
    return sqrt(dot(*this));
}

vec3 vec3::normalized() {
    return *this / length();
}

vec3 vec3::cross(vec3 other) {
    return {y * other.z - other.y * z, z * other.x - other.z * x, x * other.y - other.x * y};
}
