# 2D Shape Modeler
# Created by Courtney Linder

*You must work individually on this assignment. To receive credit, push all
required materials to your own private fork of this repository by the due date
(see the [syllabus](https://bitbucket.org/msu-cs/csci441-spring2018) for an
up-to-date schedule of assignments and due dates).*

## How to Run the program
1.)To change modes, you need to push the space bar. There are really only three modes, 
the view mode where you can scale the viewport, the stamp mode where you can stamp shapes, 
and the modify mode where you can change attributes of a shape. I didn't finish creating 
something for user definded primitives or grouping/ungrouping. Within the first mode, view
mode, you use + and - to increase and decrease the viewport.

2.)In the stamp mode, press c for circle, s for square, t for triangle, and d for done when 
you have your shape where you want to stamp it. Press m to increase rotation of the object 
and n to decrease the rotation of the object. Press "." for increasing the scale of the object 
and "," to decrease the scale of the object. These button presses need to be done before 
you press d otherwise you have to switch to modify mode to change it later.

3.)Lastly, there is the modify mode, where you can select a shape and change it's scale, rotation, 
and translation. Additionally, you cna change it's color. These transformations are accomplished 
with the same button pushes as in stamp mode.

## Write Up on Shape
I created a simple square of 4 triangles of different colors with the rotation of the square similar
to that of a diamond. I created this shape because I love colors and the way I implemented my solution,
the easiest way to really change multiple colors of a square was to make triangles and change those.
I found it interesting because it could be used in multiple situations. Since it is a simple square,
it would be relatively easy to scale it (if I could group them together, it would be even easier). Additionally,
it could be used as a symbol or as part of a flag. Flags are a way in which countries, people, organizations,
and so many more entities identify with themselves and others. Therefore a flag is a powerful representation with a lot
of meaning. So even just the idea of my sqaure being part of a flag, resonates with me. Since we may be reusing
these shapes, I wanted it to be modifiable but also meaningful.  

## Required Materials

Your program1 directory must include:

* All source code for the completed program
* A screenshot of your program displaying a shape (or collection of shapes) that
  you created using your software.
* A README.md file with any necessary instructions for using your program

## Detailed Requirements

*Your program must*

*Be an original program written by you.* You may use code from labs as a
starting point. You may talk with other students about the program, but looking
at their code is not allowed.

*Respond to mouse click and drag events.* How and where you use mouse input is
up to you.  You do not need to use mouse input only, but it must be used
somewhere.

*Be able to support different primitives.* At a minimum, you must be able to
support the primitives triangles, squares, and circles. You are encouraged to
include other shapes as well. Responding to keyboard events is probably the
easiest way to allow the user to change what shape is drawn (pressing 's' to
start drawing squares or 'c' to start drawing circles, for example, or cycling
through different available shapes by pressing 's'). It is up to you to decide
how the user is able to change the shape, but be sure to document how it is done
in your README file.

*Be able to perform linear transforms on the shapes.* Transformations to the
current shape can't affect any shapes that have already been drawn.  Be sure to
document how the user is able to transform the shapes in your README file.

*Be able to assign colors to the shapes.* At a minimum, your program must be
able to change the color of an entire shape.  You are encouraged to add
additional functionality to allow for changing the color of portions of a shape.
Be sure to document any extra color features you implement in your README file.

*Allow resizing of the window.* When the window is resized the shapes must
maintain the correct aspect ratio (no non-uniform scaling). You can decide what
coordinate space you want to store your shapes in, but you must use an
orthographic projection matrix to ensure proper scaling.

*Include a README.md file.* The README file must include any special
instructions on how to use your program. It must also include a writeup (about a
paragraph or two long) about the shape you created. Why did you create it? What
about it is interesting? What do you like about it?  You are encouraged to crate
a shape that is meaningful enough that you may want to use it as a test object
in future labs, which will help when answering the questions above.

## Point breakdown

| Points | Description |
|--------|-------------|
| 20     | View behavior
| 10     | Stamp mode behavior
| 10     | User defined primitive mode behavior
| 10     | Transformations in Stamp mode and User Defined Primitive Mode
| 10     | Group mode
| 10     | Transformations in modify mode
| 10     | Color changing in modify mode
| 20     | General (code style, execution, creativity, self expression and README.md) |
| *100*  | *Total points* |