# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/courtneylinder/Desktop/Graphics/Programs/lib/glad/src/glad.c" "/Users/courtneylinder/Desktop/Graphics/Programs/lab5/build/CMakeFiles/lab5.dir/Users/courtneylinder/Desktop/Graphics/Programs/lib/glad/src/glad.c.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../lib/glfw/include"
  "../../lib/glad/include"
  "../../lib/csci441/include"
  "/Users/courtneylinder/Desktop/Graphics/Programs/lib/glfw/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/courtneylinder/Desktop/Graphics/Programs/lab5/main.cpp" "/Users/courtneylinder/Desktop/Graphics/Programs/lab5/build/CMakeFiles/lab5.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../lib/glfw/include"
  "../../lib/glad/include"
  "../../lib/csci441/include"
  "/Users/courtneylinder/Desktop/Graphics/Programs/lib/glfw/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/courtneylinder/Desktop/Graphics/Programs/lab5/build/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
