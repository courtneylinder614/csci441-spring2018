#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;
uniform mat4 transform;
uniform mat4 viewport;
uniform mat4 world;

out vec3 myColor;

void main(){
    vec4 position = viewport*world*vec4(vec3(aPos, 1.0),1.0);
    position.z = 0.0;
    gl_Position = position;
    myColor = aColor;
}
