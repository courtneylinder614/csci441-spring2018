/*
Created By: Courtney Linder
Number: Program 1
Class: Graphics 441
*/

/*
I had help for this lab from Dan Church. He helped me with some of
the math concepts as well as some structuring for easier acces to
things.
*/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/matrix.h>
#include <csci441/uniform.h>
#include <csci441/shader.h>
#include "Tranformation.h"

enum SHAPE {
    TRIANGLE,
    SQUARE,
    CIRCLE
};

///object structre for ease of keeping track of all them
struct Object{
  Tranformation trans = Tranformation();
  GLuint vao;
  int count;
  SHAPE shape;

  void render(Shader sh){
    // draw our triangles
    Uniform::set(sh.id(), "world", trans.tranformations());
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, count);
  }
};

Tranformation viewport = Tranformation();

int SCREEN_WIDTH = 640;
int SCREEN_HEIGHT = 480;

std::vector<float> triangle;
std::vector<float> vec;
std::vector<float> square;

std::vector<Object> lists;
Object current;
Object* selected;

int user_mode = 0;

//create a function that transforms from 480,640 to -1, 1
Vector makeScreenScaled(Vector in){
  Vector out = {(2*(in.x()/SCREEN_WIDTH))-1, (2*((SCREEN_HEIGHT-in.y())/SCREEN_HEIGHT))-1, 0.0};
  return out;
}

//clamp value between min and max to make sure if fits
float clamp(float val, float min, float max){
  if(val < min){
    return min;
  }else if (val > max){
    return max;
  }else{
    return val;
  }
};

//select the object if possible
void selectObject(Vector v) {
  for (auto &object : lists) {
    if (object.trans.intersects(v)) {
      // Select the object
      selected = &object;
    }
  }
}

//create a sqaure object
std::vector<float> createSquare(){
  std::vector<float> temp ={
    0.25, 0.25, 1.0, 0.0, 0.0,
    0.25, -0.25, 1.0, 1.0, 0.0,
    -0.25, 0.25, 0.0, 1.0, 0.0,

    0.25, -0.25, 1.0, 1.0, 0.0,
    -0.25, -0.25, 0.0, 0.0, 1.0,
    -0.25, 0.25, 0.0, 1.0, 0.0
  };
  return temp;
}

//create a triangle object
std::vector<float> createTriangle(){
  std::vector<float> temp ={
    0.25, 0.25, 1.0, 0.0, 0.0,
    0.25, -0.25, 0.0, 1.0, 0.0,
    -0.25, 0.25, 0.0, 0.0, 1.0
  };
  return temp;
}

//create a circle object
std::vector<float> createCircle(){
  std::vector<float> temp;
  for(int i = 0; i < 360; i++){
    temp.push_back(cos(i*M_PI/180)/4);
    temp.push_back(sin(i*M_PI/180)/4);
    temp.push_back(1);
    temp.push_back(0);
    temp.push_back(1);

    temp.push_back(0);
    temp.push_back(0);
    temp.push_back(1);
    temp.push_back(1);
    temp.push_back(0);

    temp.push_back(cos((i + 1) * M_PI / 180)/4);
    temp.push_back(sin((i + 1) * M_PI / 180)/4);
    temp.push_back(1);
    temp.push_back(0);
    temp.push_back(0);
  }
  return temp;
}

Object useShape(std::vector<float> v, SHAPE sh) {
    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, v.size() * sizeof(float), &v[0], GL_STATIC_DRAW);

    GLuint VAO;
    // describe vertex layout
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) 0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) (2 * sizeof(float)));
    glEnableVertexAttribArray(1);
    //creating an object, set its vao, and count for rendering
    Object ob;
    ob.vao = VAO;
    ob.count = v.size()/5;
    ob.shape = sh;

    return ob;
}
//change the color of the object
void changeColor(float r, float b, float g, SHAPE sha){
  if(sha == TRIANGLE){
    vec = createTriangle();
  }else if(sha == SQUARE){
    vec = createSquare();
  }else if(sha == CIRCLE){
    vec = createCircle();
  }
  for(int i = 2; i < vec.size(); i+= 5){
    vec[i] = r;
    vec[i+1] = b;
    vec[i+2] = g;
  }
  *selected = useShape(vec, sha);
}
//function to have a callback from when the spacebar is released
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode){
    //space bar is for chaning between modes
    if(key == GLFW_KEY_SPACE && action == GLFW_RELEASE){
      user_mode ++;
      if(user_mode==2){
        current = Object();
      }
      if(user_mode > 2){
        user_mode = 0;
      }
      std::cout<<user_mode<<std::endl;
    }
    //equal is for scaling up the viewport in view mode
    else if (key == GLFW_KEY_EQUAL && action == GLFW_RELEASE){
      if(user_mode == 0){
        viewport.scale += .1;
        viewport.scale = clamp(viewport.scale, 0.01, 100);
      }
    }
    //minus is for scaling down the viewport in view mode
    else if (key == GLFW_KEY_MINUS && action == GLFW_RELEASE){
      if(user_mode == 0){
        viewport.scale -= .1;
        viewport.scale = clamp(viewport.scale, 0.01, 100);
      }
    }
    //t is for the triangle object
    else if(key == GLFW_KEY_T && action == GLFW_RELEASE){
      if(user_mode == 1){
        vec = createTriangle();
        current = useShape(vec, TRIANGLE);
      }
    }
    //s is for the square object
    else if(key == GLFW_KEY_S && action == GLFW_RELEASE){
      if(user_mode == 1){
        vec = createSquare();
        current = useShape(vec, SQUARE);
      }
    }
    //c is for the circle object
    else if(key == GLFW_KEY_C && action == GLFW_RELEASE){
      if(user_mode == 1){
        vec = createCircle();
        current = useShape(vec, CIRCLE);
      }
    }
    //period is for scaling the object up
    else if(key == GLFW_KEY_PERIOD && action == GLFW_RELEASE){
      if(user_mode == 1){
        current.trans.scale += .1;
      }
      else if(user_mode==2){
        selected->trans.scale += .1;
      }
    }
    //comman is for scaling object down
    else if(key == GLFW_KEY_COMMA && action == GLFW_RELEASE){
      if(user_mode==1){
        current.trans.scale -= .1;
      }
      else if(user_mode==2){
        selected->trans.scale -= .1;
      }
    }
    //n is for rotation in the positive direction
    else if(key == GLFW_KEY_N && action == GLFW_RELEASE){
      if(user_mode==1){
        current.trans.rotation += 3;
      }
      else if(user_mode==2){
        selected->trans.rotation += 3;
      }
    }
    //m is for rotation in the negative direction
    else if(key == GLFW_KEY_M && action == GLFW_RELEASE){
      if(user_mode==1){
        current.trans.rotation -= 3;
      }
      else if(user_mode==2){
        selected->trans.rotation -= 3;
      }
    }
    //tab allows for changing of color in modify mode
    else if(key == GLFW_KEY_TAB && action == GLFW_RELEASE){
      if(user_mode==2 and selected != NULL){
        float r, g, b;
        std::cout<<"Enter the RGB values between 0.0 and 1.0 for what color you want your shape"<<std::endl;
        std::cout<<"(Separate each value by white space)"<<std::endl;
        std::cin>> r >> g >> b;
        changeColor(r, g, b, selected->shape);
      }
    }
    //d allows you to set the object down where you want it
    else if(key == GLFW_KEY_D && action == GLFW_RELEASE){
      //mode 1 for object
      if(user_mode==1){
        lists.push_back(current);
      }
      //mode 2 for pointer object
      else if(user_mode==2){
        selected = NULL;
      }
    }
}
//cursor callback
void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    //scale the screen
    Vector v = makeScreenScaled(Vector(xpos, ypos, 0.0));
    //if in user mode 1, just be able to move object around screen
    if(user_mode==1){
      current.trans.position.values[0] = v.x();
      current.trans.position.values[1] = v.y();
    }
    //if if user mode 2, move around screen but with pointer
    else if(user_mode==2 and selected != NULL){
      selected->trans.position.values[0] = v.x();
      selected->trans.position.values[1] = v.y();
    }
}
//mouse button callback
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
  if (button == GLFW_MOUSE_BUTTON_1&& action == GLFW_RELEASE)
    {
      //check for user mode 2, modify mode
      if(user_mode == 2){
        double xpos, ypos;
        //getting cursor position
        glfwGetCursorPos(window, &xpos, &ypos);
        Vector v = makeScreenScaled(Vector(xpos, ypos, 0.0));
        //try selecting the object
        selectObject(v);
      }
    }
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int main(void) {
    GLFWwindow* window;
    /* Initialize the library */
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Program 1", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");
    //std::cout<< "Press the spacebar to edit the shape"<<std::endl;
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();
        //set uniform variable in shader to update viewport
        Uniform::set(shader.id(), "viewport", viewport.tranformations());

        //iterate over every object in list and render to screen
        for(auto object:lists) object.render(shader);
        //render curren tobject to screen
        current.render(shader);
        /* Swap front and back * buffers */
        glfwSwapBuffers(window);
        //set key board callback
        glfwSetKeyCallback(window, key_callback);
        //set cursor callback
        glfwSetCursorPosCallback(window, cursor_position_callback);
        //set mouse button callback
        glfwSetMouseButtonCallback(window, mouse_button_callback);
        /* Poll for and * process * events */
        glfwPollEvents();
    }
    glfwTerminate();
    return 0;
}
