#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <csci441/matrix.h>
#include <csci441/vector.h>

class Tranformation{
public:
  Vector position = Vector(0, 0, 0);
  float scale = 1;
  float rotation = 0;

  //func to return a matrix after performing transformations on it
  Matrix tranformations(){
    Matrix trans;
    Matrix sc;
    Matrix rot;
    trans.translate(position.x(), position.y(), 0);
    sc.scale(scale, scale, scale);
    rot.rotate_x(rotation);
    rot.rotate_y(rotation);
    rot.rotate_z(rotation);
    return trans*rot*sc;
  }
  //func to see if you are clicking the correct object to move around
  bool intersects(Vector v) {
      bool ret = false;
      float posX = position.values[0];
      float posY = position.values[1];
      float sc = scale;
      if(v.x() > posX - sc && v.x() < posX + sc && v.y() > posY - sc && v.y() < posY + sc){
        ret = true;
      }
      return ret;
  }
};

#endif
