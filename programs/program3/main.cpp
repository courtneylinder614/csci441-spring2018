/*
 * Created by Courtney Linder
 * CSCI 441 Program 3
 * Due: April 26, 2018
 */

#include <iostream>
#include <glm/glm.hpp>
#include <thread>
#include "bitmap_image.hpp"

//structure for the viewport to switch between perspective and orthogonal easily
struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max): min(min), max(max) {}
};
//sphere structure (id, center, color, radius, glazed)
struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;
    bool glazed;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0),
            bool glazed=false)
        : center(center), radius(radius), color(color), glazed(glazed) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

// Stores information and utility methods for a Ray
// The origin and direction, and where the ray is at time t
class Ray {
public:
    glm::vec3 origin;
    glm::vec3 direction;

    Ray(): origin(glm::vec3(0, 0, 0)), direction(glm::vec3(0, 0, 1)) {}
    // atTime only works if the direction is normalized, so normalize here to ensure it is normalized
    Ray(glm::vec3 origin, glm::vec3 direction): origin(origin), direction(glm::normalize(direction)) {}

    // Calculates the position on the ray at time t
    glm::vec3 atTime(float t) {
        return origin + direction * t;
    }
};

// Stores information about a Raycast Hit
// The position of the intersection and the object that was intersected with
class Hit {
public:
    glm::vec3 position;
    Sphere target;

    Hit(): position(glm::vec3(0, 0, 0)) {}
    Hit(glm::vec3 position, Sphere target): position(position), target(target) {}
};

// Determinant of quadratic equation. b^2 - 4ac
float determinant(float a, float b, float c) {
    return b * b - 4 * a * c;
}

// glm Dot product was returning a vector instead of a scalar, so wrote one that would return a scalar
float dot(glm::vec3 one, glm::vec3 two) {
    return one.x * two.x + one.y * two.y + one.z * two.z;
}

// Clamps value between two other values
float clamp(float value, float min, float max) {
    return value < min ? min : value > max ? max : value;
}

void set_pixel_color(unsigned int i, unsigned int j, bitmap_image& image, glm::vec3 color){
  const auto x = static_cast<const unsigned char>(color.x);
  const auto y = static_cast<const unsigned char>(color.y);
  const auto z = static_cast<const unsigned char>(color.z);
  image.set_pixel(i, j, x, y, z);
}

// If an object intersects a ray
bool intersects(Sphere object, Ray ray, glm::vec3 &hit) {
    // Get coefficients for quadratic equation
    // (dot(rayDir, rayDir))t^2 + dot(2 * rayDir, rayOrigin - center)t + dot(rayOrigin - center, origin - center) - R^2 = 0
    float a = dot(ray.direction, ray.direction);
    float b = dot(2.0f * ray.direction, ray.origin - object.center);
    float c = dot(ray.origin - object.center, ray.origin - object.center) - object.radius * object.radius;
    //calculate determinant
    float det = determinant(a, b, c);

    float t = 0;
    // Only continue if the determinant is >= 0. Otherwise there was not a hit
    if (det >= 0) {
        // Take the first intersection (using the minus instead of the plus in -b +- sqrt(det))
        t = (-b - std::sqrt(det)) / (2 * a);
        // Only if the object is in front of the ray (its a ray, not a line)
        if (t <= 0) return false;
        // Get the hit position
        hit = ray.atTime(t);
        return true;
    } else {
        return false;
    }
}

void phong_shading(glm::vec3& normal, glm::vec3& surfaceToLight, glm::vec3& diffuse, glm::vec3& specular, Hit& hit, glm::vec3& light){
  //normalize from the hit position to center of the target
  normal = glm::normalize(hit.position - hit.target.center);

  // Create a vector from the surface position pointing to the light
  surfaceToLight = glm::normalize(light - hit.position);

  // Calculate the diffuse amount
  float diffuseFactor = clamp(glm::dot<float, glm::highp>(surfaceToLight, normal), 0.0, 1.0);

  // Calculate specular factor amount
  float specularFactor = clamp(glm::dot<float, glm::highp>(normal, glm::reflect(surfaceToLight, normal)), 0.0, 1.0);
  specularFactor = pow(specularFactor, 32);

  // Calculate diffuse and specular
  diffuse = diffuseFactor * hit.target.color; //diffuse colour
  specular = specularFactor * hit.target.color; //specular colour

}

// Sends out a ray and determines the closes object in that ray, storing it in hit
bool raycast(const std::vector<Sphere> world, Ray ray, Hit& hit) {
    // Keep track of the minimum distance and the other hit values associated with it
    float minDist = 9999999999999;
    Hit minHit;
    // Keep track if we intersected with anything
    bool found = false;
    // Iterate through all the objects in the world
    for (auto object: world) {
        glm::vec3 hitPos;
        if (intersects(object, ray, hitPos)) {
            //get length from the hit to the ray
            float len = (hitPos - ray.origin).length();
            if (len < minDist) {
                found = true;
                minDist = len;
                minHit.target = object;
                minHit.position = hitPos;
            }
        }
    }
    hit = minHit;
    return found;
}

// Calculate a ray for a given (i, j) pair and whether to use perspective or not
Ray calculateRay(int i, int j, glm::vec3 origin, bool perspective) {
    Viewport viewport = Viewport(glm::vec2(-5, 5), glm::vec2(5, -5));
    //screen height/width
    float nx = 640;
    float ny = 480;
    //calculate u and v floats
    float u = viewport.min.x + (viewport.max.x - viewport.min.x) * (i + 0.5f) / nx;
    float v = viewport.min.y + (viewport.max.y - viewport.min.y) * (j + 0.5f) / ny;
    //make it perspective or orthogonal
    if (perspective) {
        glm::vec3 uv = glm::vec3(1, 0, 0);
        glm::vec3 vv = glm::vec3(0, 1, 0);
        float d = -origin.z;
        glm::vec3 w = glm::vec3(0, 0, 1);
        return {glm::vec3(u, v, 0), -d * w + u * uv + v * vv};
    } else {
        return {glm::vec3(u, v, 0), glm::vec3(0, 0, 1)};
    }
}
//ray trace function to call all other ray related functions
void raytrace(unsigned int i, unsigned int j, bitmap_image& image, const std::vector<Sphere>& world, glm::vec3& light, bool perspective = true) {
    //where the ray is coming from
    glm::vec3 origin = glm::vec3(0, 0, 5);
    Ray ray = calculateRay(i, j, origin, perspective);
    //hit for regular ray, one for shadow ray, and one for reflected ray
    Hit hit;
    Hit inTheShadow;
    Hit reflectHit;
    if (raycast(world, ray, hit)) {
        //variables to pass to phong shading and have populated
        glm::vec3 diffuse;
        glm::vec3 specular;
        glm::vec3 normal;
        glm::vec3 surfaceToLight;
        //method to calculate phong shading
        phong_shading(normal, surfaceToLight, diffuse, specular, hit, light);

        //shadow ray
        Ray shadow = Ray(hit.position, surfaceToLight);

        glm::vec3 reflect = glm::reflect(ray.direction, normal);

        //if it's in the shadow, color closer to the black color
        if(raycast(world, shadow, inTheShadow)){
          glm::vec3 color = hit.target.color*glm::vec3(0.01, 0.01, 0.01);
          set_pixel_color(i, j, image, color);
        }
        else if(raycast(world, Ray(hit.position, reflect), reflectHit)){
            if(hit.target.glazed == true){
              //get phong shading of reflected object
              phong_shading(normal, surfaceToLight, diffuse, specular, reflectHit, light);
              //add this diffuse and specular to color
              glm::vec3 color = (diffuse + specular);
              set_pixel_color(i, j, image, color);
          }else{
            glm::vec3 color = (diffuse + specular);
            set_pixel_color(i, j, image, color);
          }
        }
        else{
          //else just a normal color
          //create color by adding diffuse and specular (and ambient if you want)
          glm::vec3 color = (diffuse + specular);
          set_pixel_color(i, j, image, color);
        }
    }
}


void run(bitmap_image& image, const std::vector<Sphere>& world, glm::vec3& light, int startW, int endW, int startH, int endH) {
  // Ray trace
  for (unsigned int i = startW; i < endW; i++) {
      for (unsigned int j = startH; j < endH; j++) {
          raytrace(i, j, image, world, light);
      }
  }
}

void render(bitmap_image& image, const std::vector<Sphere>& world, glm::vec3& light) {
    // Set all the pixels to the sky color
    for (unsigned int i = 0; i < image.width(); i++) {
        for (unsigned int j = 0; j < image.height(); j++) {
            image.set_pixel(i, j, 75, 156, 211);
        }
    }
    //create four threads, each thread working on 1/4 of the screen
    std::thread t1 (run, std::ref(image), std::ref(world), std::ref(light), 0, image.width()/2, 0, image.height()/2);
    std::thread t2 (run, std::ref(image), std::ref(world), std::ref(light), image.width()/2, image.width(), 0, image.height()/2);
    std::thread t3 (run, std::ref(image), std::ref(world), std::ref(light), 0, image.width()/2, image.height()/2, image.height());
    std::thread t4 (run, std::ref(image), std::ref(world), std::ref(light), image.width()/2, image.width(), image.height()/2, image.height());
    //make sure to join threads at the end
    t1.join();
    t2.join();
    t3.join();
    t4.join();
}

int main(int argc, char** argv) {
    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);
    bitmap_image image2(640, 480);
    // build world
    std::vector<Sphere> world = {
        //yellow sphere
        Sphere(glm::vec3(-2, 3, 4), 1, glm::vec3(255,255,0), false),
        //cyan sphere
        Sphere(glm::vec3(2, 5, 6), 2, glm::vec3(0,255,255), false),
        //green sphere
        Sphere(glm::vec3(4, -1, 3), .5, glm::vec3(0,255,0), false),
        //violet sphere
        Sphere(glm::vec3(-2, -2, 6), 2, glm::vec3(255,0,255), false),
        //blue sphere
        Sphere(glm::vec3(1, -5, 6), .5, glm::vec3(0,0,255), false),
        //red sphere
        Sphere(glm::vec3(-4, -1, 2), .5, glm::vec3(255,0,0), false),
    };
    std::vector<Sphere> world2 = {
      Sphere(glm::vec3(-2, 1, 4), 1, glm::vec3(0,255,0), false),
      Sphere(glm::vec3(3, 0, 1), .25, glm::vec3(0,255,255), false),
      Sphere(glm::vec3(0, -1, 1), .5, glm::vec3(255,0,255), false),
      Sphere(glm::vec3(-1, 3, 1), .5, glm::vec3(255,255,0), false),
      Sphere(glm::vec3(2, 0, 3), 2, glm::vec3(255,255,255), true),
    };
    //where the light is coming from
    glm::vec3 light = glm::vec3(-6.0, 1.0, 0.0);
    //where the light is coming from
    glm::vec3 light2 = glm::vec3(0.0, 0.0, -1.0);
    // render the world
    render(image, world, light);
    image.save_image("shadows.bmp");
    //render the second world
    render(image2, world2, light2);
    image2.save_image("reflection.bmp");
    std::cout << "Success" << std::endl;
}
